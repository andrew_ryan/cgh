# Cli for efficient development in china

```
Usage: cgh -n <name>

Filter files in current folder and read if contains https://github.com , repleace https://github.com to https://ghproxy.com/https://github.com

Options:
  -n, --name        file name for filter files
  --help            display usage information
```

## Example
```
# repleace https://github.com to https://ghproxy.com/https://github.com in all Cargo.toml in current dir
cgh -n Cargo.toml 
```