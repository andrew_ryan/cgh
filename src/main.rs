#[allow(warnings)]
pub fn find(name: &str) -> Vec<String> {
    use walkdir::WalkDir;
    let mut files = vec![];
    for entry in WalkDir::new("./").into_iter().filter_map(|e| e.ok()) {
        if entry.path().is_file() {
            let path = format!("{}", entry.path().display());
            if path.ends_with(name) {
                files.push(path);
            }
        }
    }
    files
}
#[allow(warnings)]
pub fn replace(path: String) {
    let mut file_0 = std::fs::read_to_string(&path).unwrap();
    if !file_0.contains("https://ghproxy.com/https://github.com")
        && file_0.contains("https://github.com")
    {
        println!("replace https://github.com in {}", &path);
        let new_file = file_0.replace(
            "https://github.com",
            "https://ghproxy.com/https://github.com",
        );
        file_0 = new_file;
    }
    std::fs::write(path, file_0.to_string()).unwrap();
}

fn main() {
    use argh::FromArgs;

    #[derive(FromArgs)]
    /// Filter files in current folder and read if contains https://github.com , repleace https://github.com to https://ghproxy.com/https://github.com
    struct Args {
        ///file name for filter files
        #[argh(option, short = 'n')]
        name: String,
    }
    let arg: Args = argh::from_env();
    let mut re = vec![];
    let a = arg.name;
    let result = find(&a);
    re.extend_from_slice(&result);
    for path in &re {
        replace(path.to_string());
    }
}
